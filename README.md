# Noetic

## Installing Meili Agent

It is necessary to download this script and place it in the same folder where [meili-cli](https://docs.meilirobots.com/docs/get-started/on-robot-agent/installation/ros/#meili-cli) tool is located. 
Meili-cli can be downladed [here](https://meili-fms-production.s3.eu-north-1.amazonaws.com/helpers/meili-cli).

Once you have both files in the same folder you just need to run the commands below:
```
chmod +x meili-cli
chmod +x meili_agent_noetic.sh
./meili_agent_noetic.sh PIN
```

Note: PIN is the 5 digits pin code for fleet that you can generate with meili FMS [fleet configuration page](https://docs.meilirobots.com/docs/get-started/add-vehicles/ros-setup/)

Note: meili_agent will be installed in ~/catkin_ws_meili/src/

In order to use the twist multiplexor parameter defined [here](https://docs.meilirobots.com/docs/get-started/on-robot-agent/running/ros/), you need to include a true flag when running the installation script after the PIN as shown below:

```
chmod +x meili-cli
chmod +x meili_agent_noetic.sh
./meili_agent_noetic.sh PIN true
```

After finalizing the script you need to source the environment and run meili_agent as shown below: (make sure that robot is connected to the internet and it is providing the location):

```
source ~/.bashrc
roslaunch meili_agent meili_agent.launch
```

In case you activated the velocity multiplexor flag before, you should set the "vel_mux_flag" arg to true to launch the velocity multiplexor node as shown below: 

```
source ~/.bashrc
roslaunch meili_agent meili_agent.launch vel_mux_flag:=true
```

if everything has gone well, you should be able to see robot location and battery in this [page](https://app.meilirobots.com/dashboard/<your_team>/vehicles/)
